﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

	public Sprite[] hitSprites;

	private int timesHit = 0;
	private LevelManager levelManager;

	public static int brickCount = 0;

	void Start()
	{
		brickCount += 1;
		levelManager =  GameObject.FindObjectOfType<LevelManager>();
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		timesHit += 1;
		int maxHits = hitSprites.Length + 1;
		if(timesHit < maxHits)
		{
			LoadSprites();
		}
		else
		{
			brickCount -= 1;
			if (brickCount < 1) levelManager.LoadNextLevel();
			Destroy(gameObject);
		};



	}

	void LoadSprites()
	{
		//for supporting multiple damaged states, i only have one state
		int spriteIndex = timesHit - 1;

		this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];

	}

}
