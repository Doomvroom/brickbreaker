﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	private Paddle paddle;
	private Vector3 paddleToBallVector;
	private bool hasNotStarted = true;

	// Use this for initialization
	void Start () {
		paddle = GameObject.FindObjectOfType<Paddle>();
		paddleToBallVector = this.transform.position - paddle.transform.position;

	}

	// Update is called once per frame
	void Update () {
		if(hasNotStarted) {
			this.transform.position = paddle.transform.position + paddleToBallVector;

			if(Input.GetMouseButton(0)){
					hasNotStarted = false;
				this.GetComponent<Rigidbody2D>().velocity = new Vector2(3.0f, 10.0f);

			}
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		Vector2 tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
		this.GetComponent<Rigidbody2D>().velocity += tweak;
	}
}
