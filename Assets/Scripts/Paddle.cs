﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

public bool autoPlay = false;
private Ball ball;

	void Start()
	{
		ball = GameObject.FindObjectOfType<Ball>();
	}

	void Update () {
		if(!autoPlay){
			MoveWithMouse();
		}
		else{
			AutoPlay();
		}
	}

	void AutoPlay(){
		Vector3 paddlePosition = new Vector3 (6, this.transform.position.y, 0f);
		Vector3  ballPositionInBlocks = ball.transform.position;
		paddlePosition.x = Mathf.Clamp(ballPositionInBlocks.x, 0f, 15f);
		this.transform.position = paddlePosition;
	}

	void MoveWithMouse(){
		float  mousePositionInBlocks = Input.mousePosition.x / Screen.width * 16;
		mousePositionInBlocks = Mathf.Clamp(mousePositionInBlocks, 0f, 15f);
		this.transform.position = new Vector3 (mousePositionInBlocks, this.transform.position.y, 0f);
	}
}
